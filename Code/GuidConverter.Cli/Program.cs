﻿using System;

namespace GuidConverter.Cli
{
    class Program
    {
        const string RawBytes = "97A306EBE0ADBC4FB6DCDEAD1D67257D";

        static void Main(string[] args)
        {
            var byte_array = Core.GuidConverter.ParseHex(RawBytes);
            var guid = GuidConverter.Core.GuidConverter.FromRaw(RawBytes);
        }
    }
}
