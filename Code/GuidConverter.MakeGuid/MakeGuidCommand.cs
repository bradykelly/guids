﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Text;
using System.Threading.Tasks;

namespace GuidConverter.MakeGuid
{
    [Cmdlet("Make", "Guid")]
    public class MakeGuidCommand: PSCmdlet
    {
        protected override void ProcessRecord()
        {
            WriteObject(Guid.NewGuid());
        }
    }
}
